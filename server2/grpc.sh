printf "\n\n\x1b[33m================== Generate gRPC model \x1b[0m \n\n"

protoc -I . \
  -I$GOPATH/src \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  --go_out . --go_opt paths=source_relative \
  --go-grpc_out . --go-grpc_opt paths=source_relative \
  grpcDemo/demo.proto