package grpcDemo

import (
	context "context"
	"fmt"
	"log"
)

type Server struct {
	UnimplementedServer2Server
}

func (s *Server) FuncServer2(ctx context.Context, req *RequestData) (*ResponseData, error) {
	log.Println(fmt.Sprintf("Message from Server 1: %s", req.Req))
	resp := ResponseData{
		Resp: "Hello Server 1",
	}
	return &resp, nil
}
