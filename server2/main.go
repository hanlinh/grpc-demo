package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/hanlinh/grpc-demo/server2/grpcDemo"
	"google.golang.org/grpc"
)

func main() {
	port := "3333"
	// create a listener on TCP port
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", port))
	if err != nil {
		log.Println(err.Error())
	}
	// create a server instance
	s := grpcDemo.Server{}

	// create a gRPC server object
	grpcServer := grpc.NewServer()

	// attach the Ping service to the server
	grpcDemo.RegisterServer2Server(grpcServer, &s)

	// start the server
	log.Println("Server 2 is running at port: " + port)
	if err := grpcServer.Serve(lis); err != nil {
		log.Println(err.Error())
	}
}
