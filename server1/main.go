package main

import (
	"context"
	"fmt"
	"log"

	grpcServer2 "gitlab.com/hanlinh/grpc-demo/server2/grpcDemo"
	"google.golang.org/grpc"
)

func main() {
	client, connect, err := connectGRPCServer2("localhost:3333")
	if err != nil {
		log.Println(err)
	}
	defer connect.Close()
	requestData := grpcServer2.RequestData{Req: "Hello Server 2"}
	resp, err := client.FuncServer2(context.Background(), &requestData)
	if err != nil {
		log.Println(err)
	} else {
		log.Println(fmt.Sprintf("Message from Server 2: %s", resp.Resp))
	}
}

func connectGRPCServer2(endpoint string) (grpcServer2.Server2Client, *grpc.ClientConn, error) {
	var connect *grpc.ClientConn
	connect, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		return nil, nil, err
	}
	client := grpcServer2.NewServer2Client(connect)
	return client, connect, err
}
