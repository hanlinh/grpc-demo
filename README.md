# gRPC-demo

Assume we have 2 Server: Server 1 and Server 2.
Server 2 started include gRPC.

Server1 will connect and send a message to Server2. Server2 receives that message and reply to Server1.

# How to run

Run server2 and server1 in 2 terminals.

## Run grpc server in server2 folder:

First, run grpc.sh file to generate model:

```
./grpc.sh
```

Then start grpc server:

```
./run.sh
```

## Run server1:

```
./run.sh
```

## Result:

Check log in 2 terminals